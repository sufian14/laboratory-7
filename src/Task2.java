import java.util.Arrays;
public class Task2 {
	public static void main(String[] args) {
		String list[] = { "chicken", "rice", "banana", "grape", "beef", "orange" };
		int n = list.length;
		
		for (int i = 0; i < n-1; i++) {
			for (int j = 0; j < n-i-1; j++) {
				//comparing two strings based on first letters
				if (list[j+1].compareTo(list[j]) < 0) {	
					
					//swap strings 
					String temp = list[j];
					list[j] = list[j+1];
					list[j+1] = temp;
				}
			}
		}
		
		//print sorted list
		for (int i = 0; i < n; i++) 
			System.out.println(list[i]);

	}

}
